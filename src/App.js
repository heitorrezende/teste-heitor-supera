import Routes from "./routes";
import GlobalStyle from "./styles/global";
import Header from "./components/Header";
function App() {
  let products = require("./products.json");
  console.log(products);
  return (
    <>
      <Header />
      <Routes />
      <GlobalStyle />
    </>
  );
}

export default App;
