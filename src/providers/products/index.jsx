import { useEffect } from "react";
import { useState, useContext, createContext } from "react";

const ProductsContext = createContext();

export const ProductsProvider = ({ children }) => {
  const productList = require("../../products.json");
  const [products, setProducts] = useState(productList);
  const [teste, setTeste] = useState(false);
  const sortPrice = (isReverse = false) => {
    let newList = products;
    newList.sort(function (a, b) {
      return a.price - b.price;
    });

    setProducts(newList);
    if (isReverse) {
      newList.reverse();
      setProducts(newList);
    }
    setTeste(!teste);
  };
  const sortScore = (isReverse = false) => {
    let newList = products;
    newList.sort(function (a, b) {
      return a.score - b.score;
    });

    setProducts(newList);
    if (isReverse) {
      newList.reverse();
      setProducts(newList);
    }
    setTeste(!teste);
  };
  const sortName = (isReverse = false) => {
    let newList = products;
    newList.sort(function (a, b) {
      if (a.name > b.name) {
        return 1;
      }
      if (a.name < b.name) {
        return -1;
      }
      // a must be equal to b
      return 0;
    });

    setProducts(newList);
    if (isReverse) {
      newList.reverse();
      setProducts(newList);
    }
    setTeste(!teste);
  };

  useEffect(() => {}, [teste]);

  return (
    <ProductsContext.Provider
      value={{ products, setProducts, sortName, sortPrice, sortScore }}
    >
      {children}
    </ProductsContext.Provider>
  );
};
export const useProducts = () => useContext(ProductsContext);
