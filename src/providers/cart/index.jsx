import { createContext, useContext, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
const CartContext = createContext();

export const CartProvider = ({ children }) => {
  const history = useHistory();
  const [cart, setCart] = useState(
    JSON.parse(localStorage.getItem("cart")) || []
  );

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
  }, [cart]);

  const removeProduct = (index) => {
    let newCart = cart;
    let a = newCart.splice(index, 1);
    setCart(newCart);
    localStorage.setItem("cart", JSON.stringify(cart));
    return window.location.reload();
  };
  return (
    <CartContext.Provider value={{ cart, setCart, removeProduct }}>
      {children}
    </CartContext.Provider>
  );
};

export const useCart = () => useContext(CartContext);
