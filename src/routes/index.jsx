import { Switch, Route } from "react-router-dom";
import CartPage from "../pages/cart";
import DashboardPage from "../pages/dashboard";

const Routes = () => {
  return (
    <Switch>
      <Route exact path="/" component={DashboardPage} />
      <Route path="/cart" component={CartPage} />
    </Switch>
  );
};
export default Routes;
