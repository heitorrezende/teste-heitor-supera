import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";

import { FiShoppingCart } from "react-icons/fi";

import { NavLink } from "./styles";
import { Badge } from "@material-ui/core";
import { useProducts } from "../../providers/products";
import { useCart } from "../../providers/cart";

const useStyles = makeStyles((theme) => ({
  "@global": {
    ul: {
      margin: 0,
      padding: 0,
      listStyle: "none",
    },
  },
  appBar: {
    borderBottom: `1px solid ${theme.palette.divider}`,
  },
  toolbar: {
    justifyContent: "space-between",
  },
  toolbarTitle: {
    flexGrow: 1,
    fontFamily: "Nunito, sans-serif",
    fontWeight: 600,
  },
  link: {
    margin: theme.spacing(1, 1.5),
  },
}));

export default function PrimarySearchAppBar(sortButtons = false) {
  const { sortPrice, sortName, sortScore } = useProducts();

  const classes = useStyles();

  const { cart } = useCart();

  return (
    <AppBar
      position="static"
      color="default"
      elevation={0}
      className={classes.appBar}
    >
      <Toolbar className={classes.toolbar}>
        <NavLink to="/">
          <Typography
            variant="h6"
            color="inherit"
            noWrap
            className={classes.toolbarTitle}
          >
            Game Store
          </Typography>
        </NavLink>
        {sortButtons ? (
          <>
            <NavLink onClick={() => sortPrice()}>sort price Lower</NavLink>
            <NavLink onClick={() => sortPrice(true)}>sort price Higher</NavLink>
            <NavLink onClick={() => sortName()}>sort name A-Z</NavLink>
            <NavLink onClick={() => sortName(true)}>sort name Z-A</NavLink>
            <NavLink onClick={() => sortScore()}>sort lowest Score </NavLink>
            <NavLink onClick={() => sortScore(true)}>
              sort biggest Score
            </NavLink>{" "}
          </>
        ) : (
          <></>
        )}

        <nav>
          <NavLink to="/cart">
            <Badge badgeContent={cart.length} color="primary">
              <FiShoppingCart size={20} />
            </Badge>
            <span> Cart </span>
          </NavLink>
        </nav>
      </Toolbar>
    </AppBar>
  );
}
