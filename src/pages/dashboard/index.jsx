import React, { useEffect, useState } from "react";
import formatValue from "../../utils/formatValue";
import { useCart } from "../../providers/cart";
import { useProducts } from "../../providers/products";
import { Container, ProductList } from "./styles";
import Button from "@material-ui/core/Button";
const DashboardPage = () => {
  const { setCart, cart } = useCart();
  const { products, sortPrice, sortName, sortScore } = useProducts();

  return (
    <Container>
      <ProductList>
        {products.map((product) => (
          <li key={product.id}>
            <figure>
              <img src={`../../assets/${product.image}`} alt={product.name} />
            </figure>
            <strong>{product.name}</strong>
            <div>
              <span>{formatValue(product.price)}</span>

              <Button
                variant="contained"
                color="primary"
                onClick={() => setCart([...cart, product])}
              >
                <span>Add to cart</span>
              </Button>
            </div>
          </li>
        ))}
      </ProductList>
    </Container>
  );
};
export default DashboardPage;
