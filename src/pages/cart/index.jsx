import { React, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import formatValue from "../../utils/formatValue";
import { Container, Image, CardContainer, Container404 } from "./styles";

import {
  TableContainer,
  Table,
  TableHead,
  TableRow,
  Paper,
  TableCell,
  TableBody,
} from "@material-ui/core";

import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import { useHistory } from "react-router-dom";

import { useCart } from "../../providers/cart";

const useStyles = makeStyles({
  table: {
    maxWidth: 750,
    marginTop: "25px",
    margin: "15px",
    minHeight: 200,
  },
  root: {
    marginTop: "25px",
    minWidth: 275,
    maxHeight: 250,
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginTop: "15px",
    marginBottom: "15px",
    justifyContent: "center",
  },
});

const CartPage = () => {
  const { cart, removeProduct } = useCart();
  const classes = useStyles();
  const history = useHistory();
  let cartNew = [];

  useEffect(() => {
    localStorage.setItem("cart", JSON.stringify(cart));
  }, [cart]);

  const subtotal = Number(
    cart.reduce((product, acc) => acc.price + product, 0).toFixed(2)
  );

  const shipping = () => {
    if (subtotal < 250) return cart.length * 10;
    return 0;
  };
  console.log(subtotal < 200);
  if (!cart.length) {
    return (
      <Container404>
        <h1> There are no products in your cart, let`s go shopping?</h1>
        <Button
          onClick={() => history.push("/")}
          variant="contained"
          color="primary"
          size="large"
        >
          Go shop!
        </Button>
      </Container404>
    );
  }

  return (
    <>
      <Container>
        <TableContainer component={Paper} className={classes.table}>
          <Table size="small" aria-label="a dense table">
            <TableHead>
              <TableRow>
                <TableCell>
                  <strong>Product</strong>
                </TableCell>
                <TableCell>{"  "}</TableCell>
                <TableCell align="right">
                  <strong>Price</strong>
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {cart.map((product, index) => (
                <TableRow key={index}>
                  <TableCell>
                    <Image
                      src={`../../assets/${product.image}`}
                      alt="Produto"
                    />
                  </TableCell>
                  <TableCell>{product.name}</TableCell>
                  <TableCell align="right">
                    {formatValue(product.price)}
                  </TableCell>
                  <TableCell align="right">
                    <Button
                      variant="contained"
                      color="secondary"
                      onClick={() => removeProduct(index)}
                    >
                      Remove
                    </Button>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <Card className={classes.root}>
          <CardContent>
            <Typography variant="h6" component="strong">
              <strong>Your order</strong>
            </Typography>

            <CardContainer>
              <h4>
                {cart.length} Products {formatValue(subtotal)}
              </h4>

              <h4>Shipping: {formatValue(shipping())} </h4>
              <h4>Total: {formatValue(shipping() + subtotal)} </h4>
            </CardContainer>
          </CardContent>
          <CardActions className={classes.pos}>
            <Button
              className="finalizar"
              variant="contained"
              color="primary"
              size="large"
            >
              Procede to checkout
            </Button>
          </CardActions>
        </Card>
      </Container>
    </>
  );
};

export default CartPage;
